package com.minecraftonline.eggstop;

import com.google.inject.Inject;
import org.slf4j.Logger;
import org.spongepowered.api.block.BlockSnapshot;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.data.Transaction;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.block.ChangeBlockEvent;
import org.spongepowered.api.event.filter.cause.First;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.plugin.Plugin;

@Plugin(
        id = "eggstop",
        name = "EggStop",
        description = "Stop ender dragon eggs from being moved",
        authors = {"ejm"}
)
public class EggStop {

    @Inject
    private Logger logger;

    @Listener
    public void onServerStart(GameStartedServerEvent event) {
    }

    @Listener
    public void onBlockBreak(ChangeBlockEvent.Break event, @First Player player) {
        for (Transaction<BlockSnapshot> transaction : event.getTransactions()) {
            if (transaction.getOriginal().getState().getType().equals(BlockTypes.DRAGON_EGG)) {
                if (!player.hasPermission("eggstop.allow")) {
                    event.setCancelled(true);
                }
            }
        }
    }

    @Listener
    public void onBlockPlace(ChangeBlockEvent.Place event, @First Player player) {
        for (Transaction<BlockSnapshot> transaction : event.getTransactions()) {
            if (transaction.getFinal().getState().getType().equals(BlockTypes.DRAGON_EGG)) {
                if (!player.hasPermission("eggstop.allow")) {
                    event.setCancelled(true);
                }
            }
        }
    }

}
